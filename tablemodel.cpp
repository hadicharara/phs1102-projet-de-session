#include "tablemodel.h"

TableModel::TableModel(QObject *parent, NodeTableType* nodes)
    : QAbstractTableModel(parent), m_nodes(nodes)
{
}

int TableModel::rowCount(const QModelIndex & /*parent*/) const
{
   return ROWS;
}

int TableModel::columnCount(const QModelIndex & /*parent*/) const
{
    return COLUMNS;
}

QVariant TableModel::data(const QModelIndex &index, int role) const
{
    const unsigned row = index.row();
    const unsigned col = index.column();
    switch(role) {
    case Qt::DisplayRole:
        if((*m_nodes)[row][col].get_value())
            return QString("%1").arg((*m_nodes)[row][col].get_value());
        break;
    case Qt::BackgroundRole:
        return QBrush((*m_nodes)[row][col].m_gradient);
        break;
    default:
        return QVariant();
        break;
    }
    return QVariant();
}
