#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    m_model = TableModel(nullptr, &m_nodes);
    columnWidth = ui->tableView->horizontalHeader()->defaultSectionSize();
    rowHeight = ui->tableView->verticalHeader()->defaultSectionSize();
    calulateValues();
    ui->tableView->setModel(&m_model);
    ui->tableView->setItemDelegate(&m_delegate);
    ui->tableView->setRowHeight(ROWS+1, 5);
    ui->tableView->setRowHeight(0, 5);
}

double calculate_value_on_wall(double wall_1, double wall_2, double middle) {
    return (wall_1 + wall_2 + 2*middle)/4;
}

double calculate_value_middle(double value_1, double value_2, double value_3, double value_4) {
    return (value_1 + value_2 + value_3 + value_4)/4;
}

double calculate_value_corner(double value_1, double value_2) {
    return (value_1 + value_2)/2;
}

double calculate_value_depletion_corner(double wall_vertical, double wall_horizontal, double wall_vertical_opposite, double wall_horizontal_opposite) {
    return (2*wall_vertical_opposite + 2*wall_horizontal_opposite + wall_vertical + wall_horizontal)/6;
}

void MainWindow::calulateValues() {
    // initialize first row
    for (unsigned row = 0; row < ROWS; ++row) {
        for (unsigned col = 0; col < COLUMNS; ++col) {
            Node node;
            if (row == 0) {
                if(col < DEPLETION_LEFT_COL) {
                     // SOURCE
                     node.set_value(150);
                     node.set_color_green();
                }
                else if (col == DEPLETION_LEFT_COL) {
                    node.set_value(150);
                    node.set_left_green();
                }
                else if (col > DEPLETION_RIGHT_COL) {
                     // Drain
                     node.set_value(-150);
                     node.set_color_green();
                }
                else if (col == DEPLETION_RIGHT_COL) {
                    node.set_value(-150);
                    node.set_left_yellow();
                }
            }
            m_nodes[row][col] = node;
        }
    }

    bool convergence = false;
    // calculate all other rows
    for (unsigned iteration = 0; iteration < MAX_ITERATIONS && !convergence; ++iteration) {
        unsigned i = 1;
        unsigned j = 0;
        convergence = true;
        for (i = 1; i < ROWS - 1; ++i) {
            // Calculate left wall values
            m_nodes[i][j].set_value(
                    calculate_value_on_wall(
                            m_nodes[i-1][j].get_value(),
                            m_nodes[i+1][j].get_value(),
                            m_nodes[i][j+1].get_value()));
            m_nodes[i][j].set_color_green();
        }
        for (i = 1; i < ROWS - 1; ++i) {
            // Calculate green before depletion
            for (j = 1; j < DEPLETION_LEFT_COL; ++j) {
                convergence = m_nodes[i][j].set_value(
                        calculate_value_middle(
                            m_nodes[i-1][j].get_value(),
                            m_nodes[i+1][j].get_value(),
                            m_nodes[i][j+1].get_value(),
                            m_nodes[i][j-1].get_value()));
                m_nodes[i][j].set_color_green();
            }
        }

        // Bottom left corner
        m_nodes[ROWS-1][0].set_value(
                    calculate_value_corner(
                        m_nodes[ROWS-2][0].get_value(),
                        m_nodes[ROWS-1][1].get_value()));
        m_nodes[ROWS-1][0].set_color_green();

        for (j = 1; j < DEPLETION_LEFT_COL; ++j) {
            // Last row
            m_nodes[i][j].set_value(
                        calculate_value_on_wall(
                            m_nodes[i][j+1].get_value(),
                            m_nodes[i][j-1].get_value(),
                            m_nodes[i-1][j].get_value()));
            m_nodes[i][j].set_color_green();
        }

        j = DEPLETION_LEFT_COL;
        for (i = 1; i < FIRST_DEPLETION_ROW_LIMIT; ++i) {
            m_nodes[i][j].set_value(
                        calculate_value_on_wall(
                            m_nodes[i-1][j].get_value(),
                            m_nodes[i+1][j].get_value(),
                            m_nodes[i][j-1].get_value()));
            m_nodes[i][j].set_left_green();
        }
        for (i = SECOND_DEPLETION_ROW_LIMIT+1; i < ROWS - 1; ++i) {
            m_nodes[i][j].set_value(
                        calculate_value_on_wall(
                            m_nodes[i-1][j].get_value(),
                            m_nodes[i+1][j].get_value(),
                            m_nodes[i][j-1].get_value()));
            m_nodes[i][j].set_left_green();
        }

        m_nodes[ROWS-1][DEPLETION_LEFT_COL].set_value(
                    calculate_value_corner(
                        m_nodes[ROWS-2][DEPLETION_LEFT_COL].get_value(),
                        m_nodes[ROWS-1][DEPLETION_LEFT_COL-1].get_value()));
        m_nodes[ROWS-1][DEPLETION_LEFT_COL].set_left_green();

        m_nodes[FIRST_DEPLETION_ROW_LIMIT][DEPLETION_LEFT_COL].set_value(
                    calculate_value_depletion_corner(
                        m_nodes[FIRST_DEPLETION_ROW_LIMIT-1][DEPLETION_LEFT_COL].get_value(),
                        m_nodes[FIRST_DEPLETION_ROW_LIMIT][DEPLETION_LEFT_COL+1].get_value(),
                        m_nodes[FIRST_DEPLETION_ROW_LIMIT+1][DEPLETION_LEFT_COL].get_value(),
                        m_nodes[FIRST_DEPLETION_ROW_LIMIT][DEPLETION_LEFT_COL-1].get_value()));
        m_nodes[FIRST_DEPLETION_ROW_LIMIT][DEPLETION_LEFT_COL].set_color_green();

        m_nodes[SECOND_DEPLETION_ROW_LIMIT][DEPLETION_LEFT_COL].set_value(
                    calculate_value_depletion_corner(
                        m_nodes[SECOND_DEPLETION_ROW_LIMIT+1][DEPLETION_LEFT_COL].get_value(),
                        m_nodes[SECOND_DEPLETION_ROW_LIMIT][DEPLETION_LEFT_COL+1].get_value(),
                        m_nodes[SECOND_DEPLETION_ROW_LIMIT-1][DEPLETION_LEFT_COL].get_value(),
                        m_nodes[SECOND_DEPLETION_ROW_LIMIT][DEPLETION_LEFT_COL-1].get_value()));
        m_nodes[SECOND_DEPLETION_ROW_LIMIT][DEPLETION_LEFT_COL].set_color_green();

        for (i = FIRST_DEPLETION_ROW_LIMIT+1; i < SECOND_DEPLETION_ROW_LIMIT; ++i) {
            // Calculate green before depletion
            for (j = DEPLETION_LEFT_COL; j <= DEPLETION_RIGHT_COL; ++j) {
                 convergence = m_nodes[i][j].set_value(
                             calculate_value_middle(
                                 m_nodes[i-1][j].get_value(),
                                 m_nodes[i+1][j].get_value(),
                                 m_nodes[i][j+1].get_value(),
                                 m_nodes[i][j-1].get_value()));
                 m_nodes[i][j].set_color_green();
            }
        }

        i = FIRST_DEPLETION_ROW_LIMIT;
        for (j = DEPLETION_LEFT_COL+1; j < DEPLETION_RIGHT_COL; ++j) {
            m_nodes[i][j].set_value(
                        calculate_value_on_wall(
                            m_nodes[i][j+1].get_value(),
                            m_nodes[i][j-1].get_value(),
                            m_nodes[i+1][j].get_value()));
            m_nodes[i][j].set_top_yellow();
        }
        i = SECOND_DEPLETION_ROW_LIMIT;
        for (j = DEPLETION_LEFT_COL+1; j < DEPLETION_RIGHT_COL; ++j) {
            m_nodes[i][j].set_value(
                        calculate_value_on_wall(
                            m_nodes[i][j+1].get_value(),
                            m_nodes[i][j-1].get_value(),
                            m_nodes[i-1][j].get_value()));
            m_nodes[i][j].set_top_green();
        }

        m_nodes[FIRST_DEPLETION_ROW_LIMIT][DEPLETION_RIGHT_COL].set_value(
                    calculate_value_depletion_corner(
                        m_nodes[FIRST_DEPLETION_ROW_LIMIT-1][DEPLETION_RIGHT_COL].get_value(),
                        m_nodes[FIRST_DEPLETION_ROW_LIMIT][DEPLETION_RIGHT_COL-1].get_value(),
                        m_nodes[FIRST_DEPLETION_ROW_LIMIT+1][DEPLETION_RIGHT_COL].get_value(),
                        m_nodes[FIRST_DEPLETION_ROW_LIMIT][DEPLETION_RIGHT_COL+1].get_value()));
        m_nodes[FIRST_DEPLETION_ROW_LIMIT][DEPLETION_RIGHT_COL].set_color_green();

        m_nodes[SECOND_DEPLETION_ROW_LIMIT][DEPLETION_RIGHT_COL].set_value(
                    calculate_value_depletion_corner(
                        m_nodes[SECOND_DEPLETION_ROW_LIMIT+1][DEPLETION_RIGHT_COL].get_value(),
                        m_nodes[SECOND_DEPLETION_ROW_LIMIT][DEPLETION_RIGHT_COL-1].get_value(),
                        m_nodes[SECOND_DEPLETION_ROW_LIMIT-1][DEPLETION_RIGHT_COL].get_value(),
                        m_nodes[SECOND_DEPLETION_ROW_LIMIT][DEPLETION_RIGHT_COL+1].get_value()));
        m_nodes[SECOND_DEPLETION_ROW_LIMIT][DEPLETION_RIGHT_COL].set_color_green();

        j = DEPLETION_RIGHT_COL;
        for (i = 1; i < FIRST_DEPLETION_ROW_LIMIT; ++i) {
            m_nodes[i][j].set_value(
                        calculate_value_on_wall(
                            m_nodes[i-1][j].get_value(),
                            m_nodes[i+1][j].get_value(),
                            m_nodes[i][j+1].get_value()));
            m_nodes[i][j].set_left_yellow();
        }
        for (i = SECOND_DEPLETION_ROW_LIMIT+1; i < ROWS - 1; ++i) {
            m_nodes[i][j].set_value(
                        calculate_value_on_wall(
                            m_nodes[i-1][j].get_value(),
                            m_nodes[i+1][j].get_value(),
                            m_nodes[i][j+1].get_value()));
            m_nodes[i][j].set_left_yellow();
        }

        m_nodes[ROWS-1][DEPLETION_RIGHT_COL].set_value(
                    calculate_value_corner(
                        m_nodes[ROWS-2][DEPLETION_RIGHT_COL].get_value(),
                        m_nodes[ROWS-1][DEPLETION_RIGHT_COL+1].get_value()));
        m_nodes[ROWS-1][DEPLETION_RIGHT_COL].set_left_yellow();

        for (i = 1; i < ROWS - 1; ++i) {
            // Calculate green before depletion
            for (j = DEPLETION_RIGHT_COL+1; j < COLUMNS - 1; ++j) {
                convergence = m_nodes[i][j].set_value(
                            calculate_value_middle(
                                m_nodes[i-1][j].get_value(),
                                m_nodes[i+1][j].get_value(),
                                m_nodes[i][j+1].get_value(),
                                m_nodes[i][j-1].get_value()));
                m_nodes[i][j].set_color_green();
            }
        }

        i = ROWS - 1;
        for (j = DEPLETION_RIGHT_COL+1; j < COLUMNS - 1; ++j) {
            m_nodes[i][j].set_value(
                        calculate_value_on_wall(
                            m_nodes[i][j-1].get_value(),
                            m_nodes[i][j+1].get_value(),
                            m_nodes[i-1][j].get_value()));
            m_nodes[i][j].set_color_green();
        }
        j = COLUMNS - 1;
        for (i = 1; i < ROWS - 1; ++i) {
            m_nodes[i][j].set_value(
                        calculate_value_on_wall(
                            m_nodes[i-1][j].get_value(),
                            m_nodes[i+1][j].get_value(),
                            m_nodes[i][j-1].get_value()));
            m_nodes[i][j].set_color_green();
        }

        m_nodes[ROWS-1][COLUMNS-1].set_value(
                    calculate_value_corner(
                        m_nodes[ROWS-2][COLUMNS-1].get_value(),
                        m_nodes[ROWS-1][COLUMNS-2].get_value()));
        m_nodes[ROWS-1][COLUMNS-1].set_color_green();
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

