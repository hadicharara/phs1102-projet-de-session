#ifndef TABLEMODEL_H
#define TABLEMODEL_H

// mymodel.h
#include <QAbstractTableModel>
#include <QBrush>
#include <QLinearGradient>

#include "common.h"

class TableModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    TableModel(QObject *parent = nullptr, NodeTableType* nodes = nullptr);
    TableModel& operator=(TableModel&& model) noexcept {
        if (this == &model)
            return *this; // delete[]/size=0 would also be ok

        m_nodes = model.m_nodes;
        return *this;
    }
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    QLinearGradient getGradientFromEnviroments(Node& node) const;
private:
    NodeTableType* m_nodes;
};

#endif // TABLEMODEL_H
