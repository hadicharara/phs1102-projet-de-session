#ifndef CELLDELEGATE_H
#define CELLDELEGATE_H

#include <QItemDelegate>

class CellDelegate : public QItemDelegate
 {
 Q_OBJECT
 public:
 CellDelegate(QWidget *parent = 0) : QItemDelegate(parent) {}
 void paint(QPainter *painter, const QStyleOptionViewItem &option,
            const QModelIndex &index) const;
 };

#endif // CELLDELEGATE_H
