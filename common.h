#ifndef COMMON_H
#define COMMON_H

#include <QLinearGradient>
#include <cmath>

constexpr unsigned DEPLETION_LEFT_COL = 5;
constexpr unsigned DEPLETION_RIGHT_COL = 13;
constexpr unsigned FIRST_DEPLETION_ROW_LIMIT = 4;
constexpr unsigned SECOND_DEPLETION_ROW_LIMIT = 11;
constexpr unsigned GRILL_LEFT_COLUMN = 7;
constexpr unsigned GRILL_RIGHT_COLUMN = 9;
constexpr QColor YELLOW_COLOUR(239, 238, 14);
constexpr QColor GREEN_COLOUR(95, 194, 50);

constexpr unsigned MAX_ITERATIONS = 100000;

constexpr unsigned ROWS = 16;
constexpr unsigned COLUMNS = 19;

constexpr double TOLERANCE_LIMIT = 1 / 1000;

extern unsigned columnWidth;
extern unsigned rowHeight;

class Node {
public:
    Node() {
        set_color_yellow();
    }
    QLinearGradient m_gradient;
    bool set_value(double new_value) {
        bool reachedConvergence = std::abs(m_value - new_value) < TOLERANCE_LIMIT && new_value && m_value;
        m_value = new_value;
        return reachedConvergence;
    }
    double get_value() {
        return m_value;
    }
    void set_color_green() {
        m_gradient.setColorAt(0, GREEN_COLOUR);
    }
    void set_color_yellow() {
        m_gradient.setColorAt(0, YELLOW_COLOUR);
    }
    void set_left_green() {
        m_gradient = QLinearGradient(columnWidth / 2 - columnWidth * 0.15, 0, columnWidth / 2 + columnWidth * 0.15, 0);
        m_gradient.setColorAt(0, GREEN_COLOUR);
        m_gradient.setColorAt(1, YELLOW_COLOUR);
    }
    void set_left_yellow() {
        m_gradient = QLinearGradient(columnWidth / 2 + columnWidth * 0.15, 0, columnWidth / 2 - columnWidth * 0.15, 0);
        m_gradient.setColorAt(0, GREEN_COLOUR);
        m_gradient.setColorAt(1, YELLOW_COLOUR);
    }
    void set_top_green() {
        m_gradient = QLinearGradient(0, rowHeight / 2 - rowHeight * 0.15, 0, rowHeight / 2 + rowHeight * 0.15);
        m_gradient.setColorAt(0, GREEN_COLOUR);
        m_gradient.setColorAt(1, YELLOW_COLOUR);
    }
    void set_top_yellow() {
        m_gradient = QLinearGradient(0, rowHeight / 2 - rowHeight * 0.15, 0, rowHeight / 2 + rowHeight * 0.15);
        m_gradient.setColorAt(1, GREEN_COLOUR);
        m_gradient.setColorAt(0, YELLOW_COLOUR);
    }
private:
    double m_value = 0;
};

typedef Node NodeTableType [ROWS][COLUMNS];

#endif // COMMON_H
