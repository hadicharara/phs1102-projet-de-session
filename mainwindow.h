#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTableView>

#include "tablemodel.h"
#include "celldelegate.h"

extern unsigned columnWidth;
extern unsigned rowHeight;

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
public slots:
    void calulateValues();
private:
    Ui::MainWindow *ui;
    TableModel m_model;
    QItemDelegate m_delegate;
    NodeTableType m_nodes;
};
#endif // MAINWINDOW_H
