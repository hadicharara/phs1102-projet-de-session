#include "celldelegate.h"

void CellDelegate::paint(
    QPainter                     *painter,
    QStyleOptionViewItem const &  option,
        QModelIndex          const &  index) const
{
    QStyleOptionViewItem opt(option);


        opt.palette.setColor(
            QPalette::Base,
            QColor(255, 255, 204));

        opt.palette.setColor(
            QPalette::Window,
            QColor(255, 255, 204));


    return QItemDelegate::paint(painter, opt, index);
}
